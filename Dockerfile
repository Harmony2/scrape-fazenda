FROM python:3.7

WORKDIR /src/app

COPY /code/scrape.py /src/app/scrape.py
RUN pip3 install BeautifulSoup4
RUN pip3 install lxml
RUN pip3 install requests