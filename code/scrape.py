from bs4 import BeautifulSoup
import requests
import sys

estado = sys.argv[1]
if (sys.argv[2]) == "3":
    vers = "WebServices Versões 3.10 ou anteriores"
elif ((sys.argv[2]) == "4"):
    vers = "WebServices Versão 4.00"


source = requests.get('http://www.nfe.fazenda.gov.br/portal/disponibilidade.aspx?versao=0.00&tipoConteudo=Skeuqr8PQBY=').text
soup = BeautifulSoup(source, 'lxml')

a = soup.find_all("table", class_="tabelaListagemDados")
if vers in str(a[0]):
    b = a[0].find_all("tr")
elif vers in str(a[1]):
    b = a[1].find_all("tr")


holder = []


for i in b:
    holder.append(i.find_all("td"))

for i in holder:
    if (f"<td>{estado}</td>" in str(i)):
        string = str(i)
        holder = string.split(',')

for i in range(0,len(holder)):
    if holder[i] == f'[<td>{estado}</td>':
        holder[i] = estado
    elif (holder[i] ==  ' <td><img src="imagens/bola_vermelho_P.png"/></td>') | (holder[i] ==  ' <td><img src="imagens/bola_vermelho_P.png"/></td>]'):
        holder[i] = 0
    elif (holder[i] ==  ' <td><img src="imagens/bola_verde_P.png"/></td>') | (holder[i] ==  ' <td><img src="imagens/bola_verde_P.png"/></td>]'):
        holder[i] = 1
    elif (holder[i] ==  ' <td><img src="imagens/bola_amarela_P.png"/></td>') | (holder[i] ==  ' <td><img src="imagens/bola_amarela_P.png"/></td>]'):
        holder[i] = 2
    elif i == 6:
        holder[i] = 1
    elif (holder[i] == ' <td><span></span></td>') | (holder[i] == ' <td><span></span></td>]'):
        holder[i] = 1

#print(len(holder))
#print(holder)
print(f"CONSULTA AO AUTORIZADOR NFE: {holder[0]}\nAUTORIZACAO = {holder[1]}\nRETORNO AUT = {holder[2]}\nINUTILIZACAO = {holder[3]}\nCONSULTA PROTOCOLO = {holder[4]}\nSERVICO = {holder[5]}\nTEMPO = {holder[6]}\nCONSULTA CADASTRO = {holder[7]}\nRECEPCAO EVENTO = {holder[8]}")